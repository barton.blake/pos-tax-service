package com.heb.postaxservice.controller

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.heb.postaxservice.calculator.TaxCalculator
import com.heb.postaxservice.data.Item
import com.heb.postaxservice.data.Store
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/posTax")
class TaxController {
    @GetMapping("/cartTotal")
    fun calculateCartTotal(@RequestBody json: String) : ResponseEntity<Any> = runBlocking {
        try {
            val gson = Gson()
            val parsed: JsonObject = gson.fromJson(json, JsonObject::class.java)

            val store = Store(
                storeNumber = parsed.get("store").asJsonObject.get("storeNumber").asInt,
                street = parsed.get("store").asJsonObject.get("street").asString,
                city = parsed.get("store").asJsonObject.get("city").asString,
                state = parsed.get("store").asJsonObject.get("state").asString,
                zipcode = parsed.get("store").asJsonObject.get("zipcode").asString
            )
            var items = ArrayList<Item>()
            val parsedItems = parsed.get("items").asJsonArray
            for (jsonItem in parsedItems) {
                val item = Item(
                    upc = jsonItem.asJsonObject.get("upc").asInt,
                    taxable = jsonItem.asJsonObject.get("taxable").asBoolean,
                    discount = jsonItem.asJsonObject.get("discount").asDouble,
                    uniqueTax = jsonItem.asJsonObject.get("uniqueTax").asDouble,
                    voided = jsonItem.asJsonObject.get("voided").asBoolean
                )
                items.add(item)
            }

            var outD = -1.0
            val job = launch {
                outD = TaxCalculator.calculate(store, items)
            }
            job.join()

            if(outD < 0) throw Exception("Calculator did not function correctly")

            ResponseEntity.ok().body(outD)
        } catch (e: Exception) {
            ResponseEntity.badRequest().body("Exception Encountered: $e")
        }
    }
}