package com.heb.postaxservice

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.java.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import org.apache.tomcat.util.codec.binary.Base64
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

//suspend fun main(args: Array<String>) {
//    val k = KRequest()
//    k.makeRequest()
//}

class KRequest {
    val client = HttpClient(Java)

    suspend fun makeRequest() {
        val key = "#*sng_api_user*#"
        val sha256Hmac = Mac.getInstance("HmacSHA256")
        val secretKey = SecretKeySpec(key.toByteArray(), "HmacSHA256")
        sha256Hmac.init(secretKey)

        val data = "732,[4011]"
        val hash = Base64.encodeBase64String(sha256Hmac.doFinal(data.toByteArray(Charsets.UTF_8)))

        val response: HttpResponse = client.get("https://sng-cert.heb.com/sng_api/svc/v0_1/external/itemLookup/getItemDetails/732/4011") {
            headers {
                append("sng-session", hash)
            }
        }
        val responseString: String = response.receive()
        print(responseString)
    }
}