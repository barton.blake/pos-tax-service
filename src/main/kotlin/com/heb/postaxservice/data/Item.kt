package com.heb.postaxservice.data

class Item(
    val upc: Int,
    val taxable: Boolean,
    val discount: Double,
    val uniqueTax: Double,
    val voided: Boolean
    ) {
}