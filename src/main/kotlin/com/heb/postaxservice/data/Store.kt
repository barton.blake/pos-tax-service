package com.heb.postaxservice.data

class Store(
    val storeNumber: Int,
    val city: String,
    val zipcode: String,
    val state: String,
    val street: String
    ) {
    fun formatRequestString() : String {
        var out = "street=" + street.replace(" ", "%20")
        out += "&city=" + city.replace(" ", "%20")
        out += "&state=$state&zip=$zipcode"
        return out
    }
}