package com.heb.postaxservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PosTaxServiceApplication

fun main(args: Array<String>) {
	runApplication<PosTaxServiceApplication>(*args)
}
