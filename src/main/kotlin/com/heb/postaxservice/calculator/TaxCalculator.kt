package com.heb.postaxservice.calculator

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.heb.postaxservice.data.Item
import com.heb.postaxservice.data.Store
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.java.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import org.apache.tomcat.util.codec.binary.Base64
import java.math.BigDecimal
import java.math.RoundingMode
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

class TaxCalculator {
    companion object {
        private val client = HttpClient(Java)

        suspend fun calculate(store: Store, items: ArrayList<Item>) : Double {
            val taxRate = fetchTaxRate(store)
            var totalTax = 0.0
            for(item in items) {
                var tax = 0.0
                if(!item.voided && item.taxable) {
                    var itemPrice = getItemPrice(store.storeNumber, item.upc)
                    itemPrice -= item.discount
                    tax = itemPrice.times(taxRate).div(100.0)
                    tax += item.uniqueTax
                    totalTax += tax
                }
            }

            return BigDecimal(totalTax).setScale(2, RoundingMode.HALF_EVEN).toDouble()
        }

        private suspend fun fetchTaxRate(store: Store) : Double {
            val response: HttpResponse = client.post("https://sales-tax-calculator.p.rapidapi.com/rates") {
                headers {
                    append("content-type", "application/x-www-form-urlencoded")
                    append("x-rapidapi-key", "99dc96dcc8msh1d6e581d88703fbp149d79jsn07bdb2f81fcf")
                    append("x-rapidapi-host", "sales-tax-calculator.p.rapidapi.com")
                }
                body = store.formatRequestString()
            }

            val json: String = response.receive()
            val gson = Gson()
            val parsed: JsonObject = gson.fromJson(json, JsonObject::class.java)
            return parsed.get("tax_rate").asDouble
        }

        private suspend fun getItemPrice(storeNumber: Int, upc: Int) : Double {
            val key = "#*sng_api_user*#"
            val sha256Hmac = Mac.getInstance("HmacSHA256")
            val secretKey = SecretKeySpec(key.toByteArray(), "HmacSHA256")
            sha256Hmac.init(secretKey)

            val data = "$storeNumber,[$upc]"
            val hash = Base64.encodeBase64String(sha256Hmac.doFinal(data.toByteArray(Charsets.UTF_8)))

            val response: HttpResponse = client.get("https://sng-cert.heb.com/sng_api/svc/v0_1/external/itemLookup/getItemDetails/732/4011") {
                headers {
                    append("sng-session", hash)
                }
            }

            val json: String = response.receive()
            val gson = Gson()
            val parsed: JsonObject = gson.fromJson(json, JsonObject::class.java)
            return parsed.getAsJsonArray("itemModels")[0].asJsonObject.get("currentRetaillPrice").asDouble
        }
    }
}