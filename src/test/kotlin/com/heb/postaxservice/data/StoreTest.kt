package com.heb.postaxservice.data

import org.junit.jupiter.api.Test

class StoreTest {
    @Test
    fun formatString() {
        assert("street=1%20Hacker%20Way&city=Menlo%20Park&state=CA&zip=94025".equals(Store(1, "Menlo Park", "94025", "CA", "1 Hacker Way").formatRequestString()))
    }
}